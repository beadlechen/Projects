
a = array.new(1000)
a[10] = true -- 'setarray'
print(a[10]) -- 'getarray' --> true
print(#a) -- 'getsize' --> 1000

-- for i = 1, 1000 do
--     array.set(a, i, i % 2 == 0) -- a[i] = (i % 2 == 0)
-- end
-- print(array.get(a, 10)) --> true
-- print(array.get(a, 11)) --> false
-- print(array.size(a)) --> 1000

-- local b = {}
-- setmetatable(b, getmetatable(a))
-- as = array.get(b, 10)
 --> bad argument #1 to 'get' (LuaBook.array expected, got LuaBook.array)

-- print("=============== shared test start ===================")
-- print("=============== shared test end ===================")