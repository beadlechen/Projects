#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

void error (lua_State *L, const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);
    exit(EXIT_FAILURE);
}

int l_map (lua_State *L) {
    int i, n;

    /* 1st argument must be a table (t) */
    luaL_checktype(L, 1, LUA_TTABLE);

    /* 2nd argument must be a function (f) */
    luaL_checktype(L, 2, LUA_TFUNCTION);

    n = luaL_len(L, 1); /* get size of table */

    for (i = 1; i <= n; i++) {
        lua_pushvalue(L, 2); /* push f */
        lua_geti(L, 1, i); /* push t[i] */
        lua_call(L, 1, 1); /* call f(t[i]) */
        lua_seti(L, 1, i); /* t[i] = result */
    }

    return 0; /* no results */
}

static int l_split (lua_State *L) {
    const char *s = luaL_checkstring(L, 1); /* subject */
    const char *sep = luaL_checkstring(L, 2); /* separator */
    const char *e;
    int i = 1;

    lua_newtable(L); /* result table */

    /* repeat for each separator */
    while ((e = strchr(s, *sep)) != NULL) {
        lua_pushlstring(L, s, e - s); /* push substring */
        lua_rawseti(L, -2, i++); /* insert it in table */
        s = e + 1; /* skip separator */
    }

    /* insert last substring */
    lua_pushstring(L, s);
    lua_rawseti(L, -2, i);

    return 1; /* return the table */
}

/* macro to 'unsign' a character */
#define uchar(c)	((unsigned char)(c))

static int str_upper (lua_State *L) {
    size_t l;
    size_t i;
    luaL_Buffer b;
    const char *s = luaL_checklstring(L, 1, &l);
    char *p = luaL_buffinitsize(L, &b, l);
    for (i = 0; i < l; i++)
        p[i] = toupper(uchar(s[i]));
    luaL_pushresultsize(&b, l);
    return 1;
}

static int tconcat (lua_State *L) {
    luaL_Buffer b;
    int i, n;
    luaL_checktype(L, 1, LUA_TTABLE);
    n = luaL_len(L, 1);
    luaL_buffinit(L, &b);
    for (i = 1; i <= n; i++) {
        lua_geti(L, 1, i); /* get string from table */
        luaL_addvalue(&b); /* add it to the buffer */
    }
    luaL_pushresult(&b);
    return 1;
}

static int counter (lua_State *L) {
    int val = lua_tointeger(L, lua_upvalueindex(1));
    lua_pushinteger(L, ++val); /* new value */
    lua_copy(L, -1, lua_upvalueindex(1)); /* update upvalue */
    return 1; /* return new value */
}

int newCounter (lua_State *L) {
    lua_pushinteger(L, 0);
    lua_pushcclosure(L, &counter, 1);
    return 1;
}

int t_tuple (lua_State *L) {
    lua_Integer op = luaL_optinteger(L, 1, 0);
    if (op == 0) { /* no arguments? */
        int i;
        /* push each valid upvalue onto the stack */
        for (i = 1; !lua_isnone(L, lua_upvalueindex(i)); i++)
        lua_pushvalue(L, lua_upvalueindex(i));
        return i - 1; /* number of values */
    }
    else { /* get field 'op' */
        luaL_argcheck(L, 0 < op && op <= 256, 1, 
        "index out of range");
        if (lua_isnone(L, lua_upvalueindex(op)))
            return 0; /* no such field */
        lua_pushvalue(L, lua_upvalueindex(op));
        return 1;
    }
}

int t_new (lua_State *L) {
    int top = lua_gettop(L);
    luaL_argcheck(L, top < 256, top, "too many fields");
    lua_pushcclosure(L, t_tuple, top);
    return 1;
}

static const struct luaL_Reg tuplelib [] = {
    {"new", t_new},
    {NULL, NULL}
};

int luaopen_tuple (lua_State *L) {
    luaL_newlib(L, tuplelib);
    return 1;
}

int newShared (lua_State *L) {
    lua_getfield(L, lua_upvalueindex(1), "myint");
    int val = lua_tointeger(L, -1);
    lua_pushinteger(L, ++val);
    lua_setfield(L, lua_upvalueindex(1), "myint");
    lua_pushinteger(L, val); /* new value */
    return 1;
}

int newShared2 (lua_State *L) {
    lua_getfield(L, lua_upvalueindex(1), "myint");
    int val = lua_tointeger(L, -1);
    ++val;
    lua_pushinteger(L, ++val);
    lua_setfield(L, lua_upvalueindex(1), "myint");
    lua_pushinteger(L, val); /* new value */
    return 1;
}

static const struct luaL_Reg sharedlib [] = {
    {"new", newShared},
    {"new2", newShared2},
    {NULL, NULL}
};

int luaopen_shared (lua_State *L) {
    // luaL_newlib(L, sharedlib);
    /* create library table ('lib' is its list of functions) */
    luaL_newlibtable(L, sharedlib);
    /* create shared upvalue */
    lua_newtable(L);
    /* add functions in list 'lib' to the new library, sharing
    previous table as upvalue */
    luaL_setfuncs(L, sharedlib, 1);
    return 1;
}

#pragma region shared upvalues for test

static int getint(lua_State *L){
    int v = 0;
    lua_getfield(L, lua_upvalueindex(1), "myint");
    v = lua_tointeger(L, -1);
    lua_pop(L, 1); /* remove integer from stack */
    return v;
}

static void setint(lua_State *L, int v){
    lua_pushinteger(L, v);
    lua_setfield(L, lua_upvalueindex(1), "myint");
}

static int l_test(lua_State *L){
    int Global = getint(L);
    Global++;
    setint(L, Global);
    lua_pushinteger(L, Global);
    return 1;
}

static int l_anotherTest(lua_State *L){
    int Global = getint(L);
    Global++;
    Global++;
    setint(L, Global);
    lua_pushinteger(L, Global);
    return 1;
}

static const struct luaL_Reg testLib [] = {
    {"test", l_test},
    {"anotherTest", l_anotherTest},
    {NULL, NULL}
};

int luaopen_testLib(lua_State *L){
    luaL_newlibtable(L, testLib);
    lua_newtable(L);
    lua_pushinteger(L, 1);
    lua_setfield(L, -2, "myint");
    luaL_setfuncs(L, testLib, 1);
    return 1;
}

#pragma endregion

int main (void) {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L); /* opens the standard libraries */

    //lua open tuple module
    luaL_requiref(L, "tuple", luaopen_tuple, 1);
    lua_pop(L, 1);  /* remove lib */
    //lua open shared module
    luaL_requiref(L, "shared", luaopen_shared, 1);
    lua_pop(L, 1);  /* remove lib */

    luaL_requiref(L, "testLib", luaopen_testLib, 1);
    lua_pop(L, 1);  /* remove lib */

    lua_pushcfunction(L, l_split);
    lua_setglobal(L, "mysplit");

    lua_pushcfunction(L, str_upper);
    lua_setglobal(L, "mystrupper");

    lua_pushcfunction(L, tconcat);
    lua_setglobal(L, "myconcat");

    lua_pushcfunction(L, newCounter);
    lua_setglobal(L, "mynewCounter");

    // lua_getfield(L, LUA_REGISTRYINDEX, "Key");

    if (luaL_loadfile(L, "30_main.lua") || lua_pcall(L, 0, 0, 0))
        error(L, "cannot run config file: %s", lua_tostring(L, -1));

    lua_getglobal(L, "array");
    lua_getglobal(L, "array_fun");
    l_map(L);

    system("pause");

    return 0;
}