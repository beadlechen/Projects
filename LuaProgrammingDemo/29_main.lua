-- define window size

local sin = mysin(5)
print("call mysin :", sin)

local dir = mydir("D:/")
for k, v in ipairs(dir) do
    print("dir:", k, v)
end

local mylib = require "mylib"
local dir2 = mylib.dir("D:/")
for k, v in ipairs(dir2) do
    print("dir from mylib:", k, v)
end