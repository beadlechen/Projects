
for fname in mydir.open("D:/") do
    print("mydir open:", fname)
end

print("/////////////////////////////////////////////")

local lxp = require "mylxp"

local count = 0
 
callbacks = {
    StartElement = function (parser, tagname)
        io.write("+ ", string.rep(" ", count), tagname, "\n")
        count = count + 1
    end,

    EndElement = function (parser, tagname)
        count = count - 1
        io.write("- ", string.rep(" ", count), tagname, "\n")
    end,
}

p = lxp.new(callbacks) -- create new parser

for l in io.lines() do -- iterate over input lines
    assert(p:parse(l)) -- parse the line
    assert(p:parse("\n")) -- add newline
end

assert(p:parse()) -- finish document
p:close() -- close parser