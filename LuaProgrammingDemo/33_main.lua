
-- function f(x) print('global f fun call', x) end --global f fun call 5

-- function foo (x) coroutine.yield(10, x) end
 
-- function foo1 (x) foo(x + 1); return 3 end

local lproc = require("lproc")

lproc.start([[
    local test = 123;
    print("this is :", test);
]])

lproc.start([[
    local test = 123;
    print("this is2 :", test);
]])

lproc.receive("channel1")
lproc.send("channel1", "123")

lproc.exit()