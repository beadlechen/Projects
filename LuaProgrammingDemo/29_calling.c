#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <dirent.h>
#include <errno.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"


void error (lua_State *L, const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);
    exit(EXIT_FAILURE);
}

static int l_sin (lua_State *L) {
    // double d = lua_tonumber(L, 1); /* get argument */
    double d = luaL_checknumber(L, 1);
    lua_pushnumber(L, sin(d)); /* push result */
    return 1; /* number of results */
}

static int l_dir (lua_State *L) {
    DIR *dir;
    struct dirent *entry;
    int i;
    const char *path = luaL_checkstring(L, 1);

    /* open directory */
    dir = opendir(path);
    if (dir == NULL) { /* error opening the directory? */
        lua_pushnil(L); /* return nil... */
        lua_pushstring(L, strerror(errno)); /* and error message */
        return 2; /* number of results */
    }

    /* create result table */
    lua_newtable(L);
    i = 1;
    while ((entry = readdir(dir)) != NULL) { /* for each entry */
        lua_pushinteger(L, i++); /* push key */
        lua_pushstring(L, entry->d_name); /* push value */
        lua_settable(L, -3); /* table[i] = entry name */
    }

    closedir(dir);
    return 1; /* table is already on top */
}

int main (void) {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L); /* opens the standard libraries */

    lua_pushcfunction(L, l_sin);
    lua_setglobal(L, "mysin");

    lua_pushcfunction(L, l_dir);
    lua_setglobal(L, "mydir");

    if (luaL_loadfile(L, "29_main.lua") || lua_pcall(L, 0, 0, 0))
        error(L, "cannot run config file: %s", lua_tostring(L, -1));

    system("pause");

    return 0;
}