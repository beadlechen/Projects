
array = { 11, 22, 33, 44}
array_fun = function (...)
    local args = { ... }
    for k, v in ipairs(args) do
        print("array fun:", k, v)
    end
end

local ss = mysplit("hi:ho:there", ":")
for k, v in ipairs(ss) do
    print("split value:", k, v)
end

print("str upper:", mystrupper("This is lower str."))
print("concat:", myconcat({ 1, "a", 2.5}))

c1 = mynewCounter()
print(c1(), c1(), c1()) --> 1 2 3
c2 = mynewCounter()
print(c2(), c2(), c1()) --> 1 2 4

print("=============== tuple test start ===================")
t = tuple.new(2, 4, 5)
print("tuple call for1:", t(1))
-- print("tuple call for2:", t(300))
print("=============== tuple test end ===================")

print("=============== shared test start ===================")

print("shared call for1:", shared.new(), shared.new2(), shared.new2(), shared.new())

-- print("shared call for2:", testLib.test(), testLib.anotherTest())
print("shared call for2:", testLib.test(), testLib.anotherTest(), testLib.anotherTest(), testLib.test())
-- print("tuple call for2:", t(300))
print("=============== shared test end ===================")