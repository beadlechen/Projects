#coding=utf8

import os
import time
import re
import zipfile

rootdir = 'E:\logInfo\logInfo'
outputdir = 'E:\logInfo\output'
outputfile = 'E:\logInfo\output'

# extract zip file
def extractFile(path, output):
	zip_file = zipfile.ZipFile(path)
	zip_list = zip_file.namelist() # 得到压缩包里所有文件
	for f in zip_list:
		zip_file.extract(f, output) # 循环解压文件到指定目录
	zip_file.close() # 关闭文件，必须有，释放内存


# 遍历文件夹
def extractAllFiles():
    logCount = 0
    has_folders = os.listdir(outputdir) # 去重
    # print(has_folders)
    for root, dirs, files in os.walk(rootdir):
        # root 表示当前正在访问的文件夹路径
        # dirs 表示该文件夹下的子目录名list
        # files 表示该文件夹下的文件list
        # 遍历文件
        for f in files:
            #if logCount > 0 :
            #    break
            # 解压文件
            path = os.path.join(root, f)
            file_name = os.path.splitext(f)[0]
            if file_name in has_folders:
                continue
            # print(path)
            logCount = logCount + 1
            output_path = os.path.join(outputdir, file_name)
            # print(output_path)
            extractFile(path, output_path)

        # 遍历所有的文件夹
        # os.path.join(root, d)
        #for d in dirs:
        #    print(d)
        # break

    print("日志数量：", logCount)

# extract all info files
def extractInfo():
	infoCount = 1
	# 脚本名：{GetLuaFile()}，控件名：{gameObject.name} 根界面：{_rootView}
	lifePattern = '.*(脚本没有走完.*，脚本名：.*，控件名：.* 根界面：.*)'
	uiResult = {}
	for root, dirs, files in os.walk(outputdir):
		for f in files:
			#if infoCount > 10 :
			#	break
			# print(f)
			if f.startswith("info_"):
				# print("info file：", f)
				file = open(os.path.join(root, f), 'rb')
				lines = file.readlines()
				for line in lines:
					line = line.decode('utf-8')
					# print(line)
					search = re.search(lifePattern, line)
					if search :
						#print(search.group())
						# key = search.group(1) + search.group(2) + search.group(3) + search.group(4)
						if not search.group(1) in uiResult :
							uiResult[search.group(1)] = []
						uiResult[search.group(1)].append(os.path.basename(root))
				# print('line number :', len(lines))
				infoCount = infoCount + 1
	print("INFO日志数量：", infoCount)
	time_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
	out_path = outputfile + time_str
	out_txt = ""
	res_txt = ""
	for k1, v1 in uiResult.items():
		out_txt = out_txt + k1 + "\n"
		res_txt = res_txt + k1 + "\n"
		for frm in v1:
			out_txt = out_txt  + "\tfrom:" + frm + "\n"
	#print(out_txt)
	out_txt = out_txt + "\n===========================================================\n"
	out_txt = out_txt + res_txt
	out_txt = out_txt + "\n===========================================================\n"
	out_file = open(out_path, 'w')
	out_file.write(out_txt)

def main():
	try:
		extractAllFiles()
		extractInfo()
		# print(time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime()))
		#line = 'E 00:48:13 [View] [@F: 0] 脚本没有走完hide，脚本名：CardItemUI，控件名：CardItemUI 根界面：RecruitDrawCardUI(Clone)'
		#lifePattern = '.*(脚本没有走完.*，脚本名：.*，控件名：.* 根界面：.*)'
		#search = re.search(lifePattern, line)
		#print(search.group(1))
	except ZeroDivisionError as e:
		print('except:', e)
	finally:
		print('finally...')

if __name__ == '__main__':
	main()