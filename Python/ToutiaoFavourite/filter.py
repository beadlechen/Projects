#!/user/bin/python
# -*- coding: UTF-8 -*-
"""
针对是否已读（0未读，1已读），进行区分过滤

"""
import requests
import time
import csv
import hashlib
import os
import codecs
import unicodecsv
import warnings


def main():
    input_path = 'output.csv'
    output_path1 = 'read.csv'
    output_path2 = 'unread.csv'
    exists = os.path.isfile(input_path)

    write_rows = []  # 写入数据行
    if not exists:
        print('找不到' + input_path + '文件')
        return
    # 读取所有行
    input_file = open(input_path, 'r')
    reader = csv.reader(input_file)
    output_file1 = open(output_path1, 'wb')
    writer1 = csv.writer(output_file1)
    output_file2 = open(output_path2, 'wb')
    writer2 = csv.writer(output_file2)
    for line in reader:
        if line[2] == '1':
            writer1.writerow(line)
        elif line[2] == '0':
            writer2.writerow(line)
        else:
            writer1.writerow(line)
            writer2.writerow(line)


if __name__ == '__main__':
    main()
