#!/user/bin/python
# -*- coding: UTF-8 -*-
import requests
import time
import csv
import hashlib
import os
import codecs
import unicodecsv
import warnings

BASE_URL = 'https://www.toutiao.com'  # 头条基础URL

HEADERS = {
    'cookie': 'csrftoken=00b178399213808989abe3997a7f30b3; __utmz=24953151.1554035512.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __tea_sdk__ssid=7f9f7e87-1ac5-4ea6-8987-66b06cc7b68f; _ga=GA1.2.15086134.1554035512; uuid="w:1fd7624c7f3c46d091ef0b61a9f6a289"; __utma=24953151.15086134.1554035512.1558739303.1560051730.6; CNZZDATA1259612802=610227697-1554030376-https%253A%252F%252Fwww.baidu.com%252F%7C1560055800; tt_webid=6737640586180658696; __tasessionId=u4zijp19v1570029076866; tt_webid=6737640586180658696; s_v_web_id=4c854add2cd204cb40636207c8469b54; WEATHER_CITY=%E5%8C%97%E4%BA%AC; passport_auth_status=609a98881a6bb403b596a3f07e1da9c1; sso_auth_status=806c3c9d9bed9f5161404043fd4adba0; sso_uid_tt=14424fbfc75ed91920a42e7157d70025; toutiao_sso_user=b1a67130478ffa3aa9a926292a509c7f; login_flag=8cf12068f28df4b553396fe04fbc74f7; sessionid=b296da3377c4066319d12d7ac0edb4cb; uid_tt=ba2a295cffae72e9c4cb0dcff3bdf645; sid_tt=b296da3377c4066319d12d7ac0edb4cb; sid_guard="b296da3377c4066319d12d7ac0edb4cb|1570030081|15552000|Mon\054 30-Mar-2020 15:28:01 GMT"',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
}  # 通用头文件

# 获取参数as和cp
def get_ac_cp():
    e = int(time.time())
    i = hex(e).upper()[2:]
    m = hashlib.md5()
    m.update(str(e).encode(encoding='utf-8'))
    t = m.hexdigest().upper()
    if len(i) != 8:
        _as = '479BB4B7254C150'
        _cp = '7E0AC8874BB0985'
        return _as, _cp

    o = t[0:5]
    n = t[-5:]
    a = ''
    r = ''
    for s in range(5):
        a += o[s] + i[s]
        r += i[s+3] + n[s]
    _as = 'A1' + a + i[-3:]
    _cp = i[0:3] + r + 'E1'
    return _as, _cp


def main():
    input_path = 'test.csv'
    output_path = 'output.csv'
    exists = os.path.isfile(output_path)
    # 添加过滤的一些文章
    old_data = [
        'https://www.toutiao.com/item/4855597876/',  # 这样睡  睡五分钟等于六钟头
        'https://www.toutiao.com/item/5138762027/',  # 推荐24部让你脑力透支的高智商犯罪电影
        'https://www.toutiao.com/item/6185096469264122369/',  # 读完这10本书，你就能站在智商鄙视链的顶端了
        'https://www.toutiao.com/item/6196450696349254145/',  # 美国“数学哥”拒收10亿美元 凭一根网线颠覆传统十分淡定
        'https://www.toutiao.com/item/6200927993597084161/',  # 票友推荐 | 如果你还不是高级程序员 必须看看这10部电影
        'https://www.toutiao.com/item/6236219160376902145/',  # 这20本书，是15万书友力荐的“永远的枕边书”
        'https://www.toutiao.com/item/6238840534455026178/',  # 这20本书，是各领域的巅峰之作
        'https://www.toutiao.com/item/6239586791196721665/',  # 一人一本此生最爱！
        'https://www.toutiao.com/item/6254010788839686658/',  # 16岁的黑客天才，26岁他自杀了，中间是。。。
        'https://www.toutiao.com/item/6194960389940756994/',  # 看看那些喝碳酸饮料的人最后都怎么了！
        'https://www.toutiao.com/item/6204390183913259521/',  # 提升个人思想深度的11本书，每一本都令人反思良久
    ]  # 旧数据

    write_rows = []  # 写入数据行
    if exists:
        # 读取所有行
        input_file = open(output_path, 'r')
        reader = csv.reader(input_file)
        for line in reader:
            old_data.append(line[1])
    else:
        write_rows.append(['标题', '链接', '状态（0未读1已读）'])  # 如果没有旧数据，加标题

    base_url = 'https://www.toutiao.com'  # 头条基础URL
    output = open(output_path, 'ab')
    # output.seek(0, 0)
    csv_writer = unicodecsv.writer(output, encoding='utf-8')
    max_repin_time = 0
    total_count = 0  # 总条数

    while True:
        url = base_url + '/c/user/favourite/'
        _as, _cp = get_ac_cp()
        params = {
            'max_behot_time': 0,
            'count': 20,
            'page_type': 2,
            'user_id': 4288391290,  # 用户ID
            '_signature': 'New.gBAcaUJi23fbieoFaDXsP5',
            'as': _as,
            'cp': _cp,
            'max_repin_time': max_repin_time
        }
        headers = {
            'cookie': 'csrftoken=00b178399213808989abe3997a7f30b3; __utmz=24953151.1554035512.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __tea_sdk__ssid=7f9f7e87-1ac5-4ea6-8987-66b06cc7b68f; _ga=GA1.2.15086134.1554035512; uuid="w:1fd7624c7f3c46d091ef0b61a9f6a289"; __utma=24953151.15086134.1554035512.1558739303.1560051730.6; CNZZDATA1259612802=610227697-1554030376-https%253A%252F%252Fwww.baidu.com%252F%7C1560055800; tt_webid=6737640586180658696; __tasessionId=u4zijp19v1570029076866; tt_webid=6737640586180658696; s_v_web_id=4c854add2cd204cb40636207c8469b54; WEATHER_CITY=%E5%8C%97%E4%BA%AC; passport_auth_status=609a98881a6bb403b596a3f07e1da9c1; sso_auth_status=806c3c9d9bed9f5161404043fd4adba0; sso_uid_tt=14424fbfc75ed91920a42e7157d70025; toutiao_sso_user=b1a67130478ffa3aa9a926292a509c7f; login_flag=8cf12068f28df4b553396fe04fbc74f7; sessionid=b296da3377c4066319d12d7ac0edb4cb; uid_tt=ba2a295cffae72e9c4cb0dcff3bdf645; sid_tt=b296da3377c4066319d12d7ac0edb4cb; sid_guard="b296da3377c4066319d12d7ac0edb4cb|1570030081|15552000|Mon\054 30-Mar-2020 15:28:01 GMT"',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
        }
        current_time = time.time()
        resp = requests.get(url, params=params, headers=headers)
        # print("爬取数据成功:%f秒" % (time.time() - current_time))
        resp.encoding = 'utf-8'
        resp_json = resp.json()
        next_repin_time = resp_json['max_repin_time']
        data = resp_json['data']
        # print("[next_repin_time] {}".format(next_repin_time))
        count = len(data)
        if count == 0 or next_repin_time == 0:  # 旧的 max_repin_time == next_repin_time
            break
        max_repin_time = next_repin_time
        total_count += count

        for i in range(0, count):
            # print(data[i])
            title = data[i]['title']  # encode('utf-8')
            source_url = data[i]['source_url']
            # print(title)
            link_url = base_url + source_url

            un_repin(data[i])  # 取消收藏
            if link_url.encode('utf-8') not in old_data:
                print('新增条目：' + title.encode('utf-8'))
                # write_rows.append([title, link_url, 0])
                write_rows.insert(1, [title, link_url, 0])
    csv_writer.writerows(write_rows)
    print("爬取总条数：%d" % total_count)


# 取消收藏
def un_repin(data):
    url = BASE_URL + '/group/unrepin/'
    item_id = data['item_id']
    title = data['title'].encode('utf-8')
    params = {
        'item_id': item_id,
        'group_id': item_id,
    }
    resp = requests.get(url, params=params, headers=HEADERS)
    resp_json = resp.json()
    if resp_json['message'] == 'success':
        msg = '成功取消收藏:{},{}'.format(title, data['source_url'])
        print(msg)
    else:
        warnings.warn('取消收藏失败:' + data['title'])

if __name__ == '__main__':
    main()
