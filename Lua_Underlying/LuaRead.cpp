#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <lua.hpp>

// 打印对应虚拟机的当前栈
static void viewStack(lua_State *L)
{
	int i;
	int top = lua_gettop(L);
	printf("View Stack [top:%d]: ", top);
	for (int i = 1; i <= top; i++)
	{
		int t = lua_type(L, i);
		switch (t)
		{
			case LUA_TSTRING:
			{
				// string
				printf("%s", lua_tostring(L, i));
				break;
			}
			case LUA_TBOOLEAN:
			{
				//  _Bool
				printf(lua_toboolean(L, i) ? "true" : "false");
				break;
			}
			case LUA_TNUMBER:
			{
				printf("%g", lua_tonumber(L, i));
				break;
			}
			default:
			{
				printf("%s", lua_typename(L, t));
				break;
			}
		}
		printf("     ");
	}
	printf("\n");
}

const char *getStringField(lua_State *L, const char *key)
{
	const char *name;
	lua_pushstring(L, key);
	viewStack(L);
	//以栈顶元素作为key ，在lua的内部协议里使用key 和 指定索引的table获取value ，并将key弹出 value压入  此时的索引为-2
	lua_gettable(L, -2);
	printf("getStringField: \n");
	viewStack(L);
	if (!lua_isstring(L, -1))
	{
		printf("the key is not string");
	}
	name = lua_tostring(L, -1);
	lua_pop(L, 1);
	return name;
}

int CallFunc(lua_State *L, const char *key, int x, int y){
	//压入函数
	lua_getfield
	lua_getglobal(L, key);
	//压入参数 
	lua_pushnumber(L, x);
	lua_pushnumber(L, y);
	// 第二个参数是待调用函数的参数数量 第三个参数是 期望的返回结果数量 第四个参数是错误处理函数索引 
	//在获得结果之后 ，lua_pcall会先弹出栈中的函数 和 参数 然后将所得的结果按照返回顺序入栈
	if (lua_pcall(L, 2, 2, 0) != 0)
	{
		printf("function is error");
	}
	int result = lua_tonumber(L, -1); //获取存在栈顶的第一个返回结果 
	int result2 = lua_tonumber(L, -2); // 获取第二个返回结果
	printf("function result: result1=%d, result2=%d \n", result, result2);
	return result;
}

void CallFunc2(lua_State *L, const char *key)
{
	lua_getglobal(L, key);//压入函数
	if (lua_pcall(L, 0, 0, 0) != 0)
	{
		printf("error %s", lua_tostring(L, -1));
	}
}

static int TestFunc(lua_State* L)
{
	//从栈中获取函数的参数 
	int x = lua_tonumber(L, 1);
	int y = lua_tonumber(L, 2);
 
    //将函数的返回结果进行入栈 
	lua_pushnumber(L, x + y);
	lua_pushnumber(L, x - y);
	// 函数返回结果的数量
	return 2;
}

int main()
{
	int error;
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);

	//加载test.lua文件 并进行编译
	error = luaL_loadfile(L, "test.lua") || lua_pcall(L, 0, 0, 0);
	if (error)
	{
		//如果test.lua文件发生错误，会将错误信息入栈，取出错误信息并输出
		fprintf(stderr, "%s", lua_tostring(L, -1));
		lua_pop(L, 1);
	}

	//将全局变量 width  height people分别 入栈
	lua_getglobal(L, "width");
	lua_getglobal(L, "height");
	lua_getglobal(L, "people");
	// 按照固定索引取出对应的元素 ，此处应使用lua_is...函数进行判断，再取出
	int w = lua_tointeger(L, -3);
	int h = lua_tointeger(L, -2);

	//取出 people 里key = “age”的变量值
	//int age = lua_getfield(L, -1, "age");  // 使用age直接接收返回值是错误的， lua_getfield返回的是对应的数据类型，此处为Lua_Number,对应lua源码里的LUAT_NUMBER(3).下面是正确的做法
	lua_getfield(L, -1, "age");	   //将people里key = “age”的变量值入栈，也就是说此时栈顶是13.
	viewStack(L);				   // 对应输出 800 600 table 13
	int age = lua_tonumber(L, -1); //
	lua_pop(L, 1);				   // 出栈一个元素 ，也就是13.
	// 取出people 里 key = “name”的变量值 （该函数是我们自己实现了类似lua_getfield的功能）当前栈结构 800 600 table
	const char *name = getStringField(L, "name");

	printf("w=%d, h=%d \n", w, h);
	printf("age is %d, name is %s \n", age, name);

	//C调用lua的函数
	printf("---------- C Call Lua Function ------------ \n");
	CallFunc(L, "f", 5, 3);
	//Lua调用C的函数
	printf("---------- Lua Call C Function ------------ \n");
	//将函数注册到Lua中 
	lua_pushcfunction(L, TestFunc);
	lua_setglobal(L, "func");
	CallFunc2(L, "f2");

	system("pause");
	return 0;
}