﻿using System;
using System.Runtime.InteropServices;

namespace CSharp
{
    class Program
    {
        private const string LUADLL = "lua53";

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr luaL_newstate();

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void luaL_openlibs(IntPtr luaState);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern int luaL_loadfilex(IntPtr luaState, string filename, string mode);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern int lua_pcallk(IntPtr luaState, int nArgs, int nResults, int errfunc, int a, IntPtr b);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void lua_getglobal(IntPtr luaState, string name);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern int lua_tointegerx(IntPtr luaState, int idx, int a);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void lua_pushnumber(IntPtr luaState, double number);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void lua_newuserdata(IntPtr luaState, int size);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern double lua_tonumberx(IntPtr luaState, int idx, int a);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr lua_touserdata(IntPtr luaState, int idx);
        
        static void Main(string[] args)
        {
            IntPtr L = luaL_newstate();
            luaL_openlibs(L);

            int error = luaL_loadfilex(L, "test.lua", null) | lua_pcallk(L, 0, 0, 0, 0, IntPtr.Zero);

            Console.WriteLine("Load lua file:" + error);

            //将全局变量 width  height people分别 入栈
            lua_getglobal(L, "width");
            lua_getglobal(L, "height");
            lua_getglobal(L, "people");

            // 按照固定索引取出对应的元素 ，此处应使用lua_is...函数进行判断，再取出
            int w = lua_tointegerx(L, -3, 0);
            int h = lua_tointegerx(L, -2, 0);

            Console.WriteLine("Hello World!" + w + ":" + h);

            CallFunc(L, "f", 5, 4);

            Test t = new Test(123, 111);
            Test t2 = CallFuncTest(L, t);
            Console.WriteLine("Test" + t2.a + ":" + t2.b);
        }

        static double CallFunc(IntPtr L, string key, int x, int y){
            //压入函数
            lua_getglobal(L, key);
            //压入参数 
            lua_pushnumber(L, x);
            lua_pushnumber(L, y);
            // 第二个参数是待调用函数的参数数量 第三个参数是 期望的返回结果数量 第四个参数是错误处理函数索引 
            //在获得结果之后 ，lua_pcall会先弹出栈中的函数 和 参数 然后将所得的结果按照返回顺序入栈
            if (lua_pcallk(L, 2, 2, 0, 0, IntPtr.Zero) != 0)
            {
                Console.WriteLine("function is error");
            }
            double result = lua_tonumberx(L, -1, 0); //获取存在栈顶的第一个返回结果 
            double result2 = lua_tonumberx(L, -2, 0); // 获取第二个返回结果
            Console.WriteLine("function result:" + result +":"+ result2);
            return result;
        }

        static Test CallFuncTest(IntPtr L, Test a){
            lua_getglobal(L, "test");
            
            lua_newuserdata(L, sizeof(int));
            if (lua_pcallk(L, 1, 1, 0, 0, IntPtr.Zero) != 0)
            {
                Console.WriteLine("function is error");
            }
            object rs = lua_touserdata(L, -1);
            return (Test)rs;
        } 
    }

    class Test {
        public int a = 1;
        public int b = 2;

        public Test() {}

        public Test(int v1, int v2){
            this.a = v1;
            this.b = v2;
        }

        public int TestMethod(){
            return a + b;
        }
    }
}
