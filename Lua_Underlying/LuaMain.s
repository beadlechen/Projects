	.file	"LuaMain.cpp"
.lcomm __ZStL8__ioinit,1,1
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "hello world\0"
LC2:
	.ascii "%d %s \12\0"
LC3:
	.ascii "pause\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB1023:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$48, %esp
	call	___main
	call	_luaL_newstate
	movl	%eax, 44(%esp)
	movl	$LC0, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_pushstring
	fldl	LC1
	fstpl	4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_pushnumber
	movl	$-1, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_isnumber
	testl	%eax, %eax
	je	L2
	movl	$-2, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_isstring
	testl	%eax, %eax
	je	L2
	movl	$1, %eax
	jmp	L3
L2:
	movl	$0, %eax
L3:
	testb	%al, %al
	je	L4
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	30(%esp)
	movzwl	30(%esp), %eax
	movb	$12, %ah
	movw	%ax, 28(%esp)
	fldcw	28(%esp)
	fistpl	40(%esp)
	fldcw	30(%esp)
	movl	$0, 8(%esp)
	movl	$-2, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tolstring
	movl	%eax, 36(%esp)
	movl	$-3, 4(%esp)
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_settop
	movl	36(%esp), %eax
	movl	%eax, 8(%esp)
	movl	40(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC2, (%esp)
	call	_printf
L4:
	movl	44(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_close
	movl	$LC3, (%esp)
	call	_system
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1023:
	.def	___tcf_0;	.scl	3;	.type	32;	.endef
___tcf_0:
LFB1025:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitD1Ev
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1025:
	.def	__Z41__static_initialization_and_destruction_0ii;	.scl	3;	.type	32;	.endef
__Z41__static_initialization_and_destruction_0ii:
LFB1024:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$1, 8(%ebp)
	jne	L9
	cmpl	$65535, 12(%ebp)
	jne	L9
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitC1Ev
	movl	$___tcf_0, (%esp)
	call	_atexit
L9:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1024:
	.def	__GLOBAL__sub_I_main;	.scl	3;	.type	32;	.endef
__GLOBAL__sub_I_main:
LFB1026:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$65535, 4(%esp)
	movl	$1, (%esp)
	call	__Z41__static_initialization_and_destruction_0ii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1026:
	.section	.ctors,"w"
	.align 4
	.long	__GLOBAL__sub_I_main
	.section .rdata,"dr"
	.align 8
LC1:
	.long	0
	.long	1076101120
	.ident	"GCC: (GNU) 5.3.0"
	.def	_luaL_newstate;	.scl	2;	.type	32;	.endef
	.def	_lua_pushstring;	.scl	2;	.type	32;	.endef
	.def	_lua_pushnumber;	.scl	2;	.type	32;	.endef
	.def	_lua_isnumber;	.scl	2;	.type	32;	.endef
	.def	_lua_isstring;	.scl	2;	.type	32;	.endef
	.def	_lua_tonumberx;	.scl	2;	.type	32;	.endef
	.def	_lua_tolstring;	.scl	2;	.type	32;	.endef
	.def	_lua_settop;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_lua_close;	.scl	2;	.type	32;	.endef
	.def	_system;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	_atexit;	.scl	2;	.type	32;	.endef
