	.file	"LuaRead.cpp"
.lcomm __ZStL8__ioinit,1,1
	.section .rdata,"dr"
LC0:
	.ascii "View Stack [top:%d]: \0"
LC1:
	.ascii "%s\0"
LC2:
	.ascii "true\0"
LC3:
	.ascii "false\0"
LC4:
	.ascii "%g\0"
LC5:
	.ascii "     \0"
	.text
	.def	__ZL9viewStackP9lua_State;	.scl	3;	.type	32;	.endef
__ZL9viewStackP9lua_State:
LFB1023:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_gettop
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_printf
	movl	$1, -12(%ebp)
L10:
	movl	-12(%ebp), %eax
	cmpl	-16(%ebp), %eax
	jg	L2
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_type
	movl	%eax, -20(%ebp)
	movl	-20(%ebp), %eax
	cmpl	$3, %eax
	je	L4
	cmpl	$4, %eax
	je	L5
	cmpl	$1, %eax
	je	L6
	jmp	L11
L5:
	movl	$0, 8(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tolstring
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	jmp	L7
L6:
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_toboolean
	testl	%eax, %eax
	je	L8
	movl	$LC2, %eax
	jmp	L9
L8:
	movl	$LC3, %eax
L9:
	movl	%eax, (%esp)
	call	_printf
	jmp	L7
L4:
	movl	$0, 8(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fstpl	4(%esp)
	movl	$LC4, (%esp)
	call	_printf
	jmp	L7
L11:
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_typename
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	nop
L7:
	movl	$LC5, (%esp)
	call	_printf
	addl	$1, -12(%ebp)
	jmp	L10
L2:
	movl	$10, (%esp)
	call	_putchar
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1023:
	.section .rdata,"dr"
LC7:
	.ascii "getStringField: \0"
LC8:
	.ascii "the key is not string\0"
	.text
	.globl	__Z14getStringFieldP9lua_StatePKc
	.def	__Z14getStringFieldP9lua_StatePKc;	.scl	2;	.type	32;	.endef
__Z14getStringFieldP9lua_StatePKc:
LFB1024:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pushstring
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	__ZL9viewStackP9lua_State
	movl	$-2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_gettable
	movl	$LC7, (%esp)
	call	_puts
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	__ZL9viewStackP9lua_State
	movl	$-1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_isstring
	testl	%eax, %eax
	sete	%al
	testb	%al, %al
	je	L13
	movl	$LC8, (%esp)
	call	_printf
L13:
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tolstring
	movl	%eax, -12(%ebp)
	movl	$-2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_settop
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1024:
	.section .rdata,"dr"
LC9:
	.ascii "function is error\0"
	.align 4
LC10:
	.ascii "function result: result1=%d, result2=%d \12\0"
	.text
	.globl	__Z8CallFuncP9lua_StatePKcii
	.def	__Z8CallFuncP9lua_StatePKcii;	.scl	2;	.type	32;	.endef
__Z8CallFuncP9lua_StatePKcii:
LFB1025:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$72, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_getglobal
	fildl	16(%ebp)
	fstpl	4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pushnumber
	fildl	20(%ebp)
	fstpl	4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pushnumber
	movl	$0, 20(%esp)
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	$2, 8(%esp)
	movl	$2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pcallk
	testl	%eax, %eax
	setne	%al
	testb	%al, %al
	je	L16
	movl	$LC9, (%esp)
	call	_printf
L16:
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	-26(%ebp)
	movzwl	-26(%ebp), %eax
	movb	$12, %ah
	movw	%ax, -28(%ebp)
	fldcw	-28(%ebp)
	fistpl	-12(%ebp)
	fldcw	-26(%ebp)
	movl	$0, 8(%esp)
	movl	$-2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	-26(%ebp)
	movzwl	-26(%ebp), %eax
	movb	$12, %ah
	movw	%ax, -28(%ebp)
	fldcw	-28(%ebp)
	fistpl	-16(%ebp)
	fldcw	-26(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$LC10, (%esp)
	call	_printf
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1025:
	.section .rdata,"dr"
LC11:
	.ascii "error %s\0"
	.text
	.globl	__Z9CallFunc2P9lua_StatePKc
	.def	__Z9CallFunc2P9lua_StatePKc;	.scl	2;	.type	32;	.endef
__Z9CallFunc2P9lua_StatePKc:
LFB1026:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_getglobal
	movl	$0, 20(%esp)
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pcallk
	testl	%eax, %eax
	setne	%al
	testb	%al, %al
	je	L20
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tolstring
	movl	%eax, 4(%esp)
	movl	$LC11, (%esp)
	call	_printf
L20:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1026:
	.def	__ZL8TestFuncP9lua_State;	.scl	3;	.type	32;	.endef
__ZL8TestFuncP9lua_State:
LFB1027:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	$0, 8(%esp)
	movl	$1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	-26(%ebp)
	movzwl	-26(%ebp), %eax
	movb	$12, %ah
	movw	%ax, -28(%ebp)
	fldcw	-28(%ebp)
	fistpl	-12(%ebp)
	fldcw	-26(%ebp)
	movl	$0, 8(%esp)
	movl	$2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	-26(%ebp)
	movzwl	-26(%ebp), %eax
	movb	$12, %ah
	movw	%ax, -28(%ebp)
	fldcw	-28(%ebp)
	fistpl	-16(%ebp)
	fldcw	-26(%ebp)
	movl	-12(%ebp), %edx
	movl	-16(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -32(%ebp)
	fildl	-32(%ebp)
	fstpl	4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pushnumber
	movl	-12(%ebp), %eax
	subl	-16(%ebp), %eax
	movl	%eax, -32(%ebp)
	fildl	-32(%ebp)
	fstpl	4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lua_pushnumber
	movl	$2, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1027:
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC12:
	.ascii "test.lua\0"
LC13:
	.ascii "width\0"
LC14:
	.ascii "height\0"
LC15:
	.ascii "people\0"
LC16:
	.ascii "age\0"
LC17:
	.ascii "name\0"
LC18:
	.ascii "w=%d, h=%d \12\0"
LC19:
	.ascii "age is %d, name is %s \12\0"
	.align 4
LC20:
	.ascii "---------- C Call Lua Function ------------ \0"
LC21:
	.ascii "f\0"
	.align 4
LC22:
	.ascii "---------- Lua Call C Function ------------ \0"
LC23:
	.ascii "func\0"
LC24:
	.ascii "f2\0"
LC25:
	.ascii "pause\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB1028:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$80, %esp
	call	___main
	call	_luaL_newstate
	movl	%eax, 76(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_luaL_openlibs
	movl	$0, 8(%esp)
	movl	$LC12, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_luaL_loadfilex
	testl	%eax, %eax
	jne	L24
	movl	$0, 20(%esp)
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_pcallk
	testl	%eax, %eax
	je	L25
L24:
	movl	$1, %eax
	jmp	L26
L25:
	movl	$0, %eax
L26:
	movzbl	%al, %eax
	movl	%eax, 72(%esp)
	cmpl	$0, 72(%esp)
	je	L27
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tolstring
	movl	%eax, %edx
	movl	__imp___iob, %eax
	addl	$64, %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_fputs
	movl	$-2, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_settop
L27:
	movl	$LC13, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_getglobal
	movl	$LC14, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_getglobal
	movl	$LC15, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_getglobal
	movl	$0, 8(%esp)
	movl	$-3, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tointegerx
	movl	%eax, 68(%esp)
	movl	$0, 8(%esp)
	movl	$-2, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tointegerx
	movl	%eax, 64(%esp)
	movl	$LC16, 8(%esp)
	movl	$-1, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_getfield
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	__ZL9viewStackP9lua_State
	movl	$0, 8(%esp)
	movl	$-1, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_tonumberx
	fnstcw	46(%esp)
	movzwl	46(%esp), %eax
	movb	$12, %ah
	movw	%ax, 44(%esp)
	fldcw	44(%esp)
	fistpl	60(%esp)
	fldcw	46(%esp)
	movl	$-2, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_settop
	movl	$LC17, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	__Z14getStringFieldP9lua_StatePKc
	movl	%eax, 56(%esp)
	movl	64(%esp), %eax
	movl	%eax, 8(%esp)
	movl	68(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC18, (%esp)
	call	_printf
	movl	56(%esp), %eax
	movl	%eax, 8(%esp)
	movl	60(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC19, (%esp)
	call	_printf
	movl	$LC20, (%esp)
	call	_puts
	movl	$3, 12(%esp)
	movl	$5, 8(%esp)
	movl	$LC21, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	__Z8CallFuncP9lua_StatePKcii
	movl	$LC22, (%esp)
	call	_puts
	movl	$0, 8(%esp)
	movl	$__ZL8TestFuncP9lua_State, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_pushcclosure
	movl	$LC23, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	_lua_setglobal
	movl	$LC24, 4(%esp)
	movl	76(%esp), %eax
	movl	%eax, (%esp)
	call	__Z9CallFunc2P9lua_StatePKc
	movl	$LC25, (%esp)
	call	_system
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1028:
	.def	___tcf_0;	.scl	3;	.type	32;	.endef
___tcf_0:
LFB1030:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitD1Ev
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1030:
	.def	__Z41__static_initialization_and_destruction_0ii;	.scl	3;	.type	32;	.endef
__Z41__static_initialization_and_destruction_0ii:
LFB1029:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$1, 8(%ebp)
	jne	L32
	cmpl	$65535, 12(%ebp)
	jne	L32
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitC1Ev
	movl	$___tcf_0, (%esp)
	call	_atexit
L32:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1029:
	.def	__GLOBAL__sub_I__Z14getStringFieldP9lua_StatePKc;	.scl	3;	.type	32;	.endef
__GLOBAL__sub_I__Z14getStringFieldP9lua_StatePKc:
LFB1031:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$65535, 4(%esp)
	movl	$1, (%esp)
	call	__Z41__static_initialization_and_destruction_0ii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1031:
	.section	.ctors,"w"
	.align 4
	.long	__GLOBAL__sub_I__Z14getStringFieldP9lua_StatePKc
	.ident	"GCC: (GNU) 5.3.0"
	.def	_lua_gettop;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_lua_type;	.scl	2;	.type	32;	.endef
	.def	_lua_tolstring;	.scl	2;	.type	32;	.endef
	.def	_lua_toboolean;	.scl	2;	.type	32;	.endef
	.def	_lua_tonumberx;	.scl	2;	.type	32;	.endef
	.def	_lua_typename;	.scl	2;	.type	32;	.endef
	.def	_putchar;	.scl	2;	.type	32;	.endef
	.def	_lua_pushstring;	.scl	2;	.type	32;	.endef
	.def	_lua_gettable;	.scl	2;	.type	32;	.endef
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_lua_isstring;	.scl	2;	.type	32;	.endef
	.def	_lua_settop;	.scl	2;	.type	32;	.endef
	.def	_lua_getglobal;	.scl	2;	.type	32;	.endef
	.def	_lua_pushnumber;	.scl	2;	.type	32;	.endef
	.def	_lua_pcallk;	.scl	2;	.type	32;	.endef
	.def	_luaL_newstate;	.scl	2;	.type	32;	.endef
	.def	_luaL_openlibs;	.scl	2;	.type	32;	.endef
	.def	_luaL_loadfilex;	.scl	2;	.type	32;	.endef
	.def	_fputs;	.scl	2;	.type	32;	.endef
	.def	_lua_tointegerx;	.scl	2;	.type	32;	.endef
	.def	_lua_getfield;	.scl	2;	.type	32;	.endef
	.def	_lua_pushcclosure;	.scl	2;	.type	32;	.endef
	.def	_lua_setglobal;	.scl	2;	.type	32;	.endef
	.def	_system;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	_atexit;	.scl	2;	.type	32;	.endef
