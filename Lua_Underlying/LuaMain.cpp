#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <lua.hpp>

int main()
{
	//Lua库中没有定义任何全局变量。它将所有的状态都保存在动态结构lua_State中，所有的C API都要求传入一个指向该结构的指针。
	//luaL_newstate函数用于创建一个新环境或状态,也就是所谓的lua虚拟机对象。
	lua_State* L = luaL_newstate();
	//将hello world 压入虚拟栈 
	lua_pushstring(L, "hello world");
	//将整数10压入虚拟栈
	lua_pushnumber(L, 10);
	lua_touserdata
	if (lua_isnumber(L, -1) && lua_isstring(L, -2))
	{
		//虚拟栈对应位置的元素转换为对应类型 
		int num = lua_tonumber(L, -1);
		const char* str = lua_tostring(L, -2);
        lua_pop(L,2); // 出栈两个元素 
		printf("%d %s \n", num, str);
	}
	lua_close(L);
	system("pause");

	return 0;
}