﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCodEnum : MonoBehaviour
{
    public enum TimeOfDay
    {
        Moning = 0,
        Afternoon = 1,
        Evening = 2,
    };
    // Use this for initialization
    void Start() { }
    private void TestDic()
    {
        Dictionary<TimeOfDay, int> dicData = new Dictionary<TimeOfDay, int>();
        dicData.Add(TimeOfDay.Evening, 1);
    }
    private void TestStr()
    {
        string mm = TimeOfDay.Evening.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        // TestDic();
        TestStr();
    }
}