﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
public class TestCodPerfomanceStructClass : MonoBehaviour
{
    public struct PathStruct
    {
        public string path;
    }
    public class PathClass
    {
        public string path;
    }
    List<PathStruct> PathStructList = new List<PathStruct>();
    List<PathClass> PathClassList = new List<PathClass>();
    List<PathStruct> PathStructModifyList = new List<PathStruct>();
    // Use this for initialization
    void Start()
    {
        PathStruct mPathStruct = new PathStruct();
        mPathStruct.path = "def";
        PathStructModifyList.Add(mPathStruct);

        //测试坐标Vector
        // transform.localPosition = Vector3.one;
        var pos = transform.localPosition;
        pos.x = 100;
        transform.localPosition = pos;
    }
    void testStructClass_struct()
    {
        PathStruct mPathStruct = new PathStruct();
        mPathStruct.path = "abc";
        PathStructList.Add(mPathStruct);
    }
    void testStructClass_class()
    {
        PathClass mPathStruct = new PathClass();
        mPathStruct.path = "abcdef";
        PathClassList.Add(mPathStruct);
    }
    void testStructClass_struct_modifyData()
    {
        PathStruct mPathStruct = PathStructModifyList[0];
        mPathStruct.path = "new";
        // Debug.Log(PathStructModifyList[0].path);
        PathStructModifyList[0] = mPathStruct;
        // Debug.Log(PathStructModifyList[0].path);
        //PathStructModifyList.RemoveAt(0);
        //PathStructModifyList.Add(mPathStruct);
    }
    // Update is called once per frame
    void Update()
    {
        //testStructClass_struct();
        //testStructClass_class();
        testStructClass_struct_modifyData();
    }
}