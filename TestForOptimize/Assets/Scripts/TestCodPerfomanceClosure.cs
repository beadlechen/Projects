﻿using UnityEngine;
using System.Collections;
using System;
public class TestCodPerfomanceClosure : MonoBehaviour
{
    void Start() { }
    // Update is called once per frame
    void Update()
    {
        int value = 3;
        //TestDelegate testFunc = ((int index) =>
        //{
        // return (++value) * index;
        //});
        //int res = testFunc(4); //16
        //int res1 = testFunc(4); //20
        Action action = () =>
        {
            int y = 2;
            int result = value + y;
        };
        action();
    }
}