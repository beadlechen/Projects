﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
public class TestCodPerfomanceStructClassArrayList : MonoBehaviour
{
    public struct PathStruct
    {
        public string path;
    }
    public interface IPathStructInterface
    {
        string path { get; set; }
    }
    public struct PathStructInterface : IPathStructInterface
    {
        private string _path;
        public string path
        {
            get { return _path; }
            set { _path = value; }
        }
    }
    ArrayList PathStructList = new ArrayList();
    ArrayList PathStructListInterface = new ArrayList();
    ArrayList PathStructModifyList = new ArrayList();
    ArrayList PathStructModifyInterfaceList = new ArrayList();
    // Use this for initialization
    void Start()
    {
        PathStruct mPathStruct = new PathStruct();
        mPathStruct.path = "def";
        PathStructModifyList.Add(mPathStruct);
        PathStructInterface mPathStructInterface = new PathStructInterface();
        mPathStructInterface.path = "def";
        PathStructModifyInterfaceList.Add(mPathStructInterface);
    }
    void testStructClass_struct()
    {
        PathStruct mPathStruct = new PathStruct();
        mPathStruct.path = "abc";
        PathStructList.Add(mPathStruct);
    }
    void testStructClass_struct_interface()
    {
        PathStructInterface mPathStructInterface = new PathStructInterface();
        mPathStructInterface.path = "abc";
        PathStructListInterface.Add(mPathStructInterface);
    }
    void testStructClass_struct_modifyData()
    {
        PathStruct mPathStruct = (PathStruct)PathStructModifyList[0];
        mPathStruct.path = "new";
        PathStructModifyList[0] = mPathStruct;
        //PathStructModifyList.RemoveAt(0);
        //PathStructModifyList.Add(mPathStruct);
    }
    void testStructClass_struct_modifyData_interface()
    {
        IPathStructInterface mPathStruct = (IPathStructInterface)PathStructModifyInterfaceList[0];
        mPathStruct.path = "new";
        PathStructModifyInterfaceList[0] = mPathStruct;
    }
    // Update is called once per frame
    void Update()
    {
        testStructClass_struct();
        testStructClass_struct_interface();
        testStructClass_struct_modifyData();
        testStructClass_struct_modifyData_interface();
    }
}