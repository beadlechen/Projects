#include "main.h"

// a sample exported function
DLL_EXPORT int test(int a, int b)
{
// 测试编译参数 -DBUILD_DLL
// #ifdef BUILD_DLL
//     printf("This is export \n");
// #else
//     printf("This is import  \n");
// #endif  
    return a + b;
}
