DLL是啥，如何生成，与C/C++/C#之间的关系，如果相互调用的？

* C/C++语言加载DLL分别有静态和动态（LoadLibrary）加载方式。

```shell
C程序编译DLL的过程
注意：因为代码中有用 #ifdef BUILD_DLL 判断导入还是导出，如果是导出需要加参数-DBUILD_DLL
gcc -DBUILD_DLL -c main.c # compile and assemble to main.o
gcc -shared -o DLL_Test.dll -Wl,--dll main.o # compile to a dll file
gcc -o C_With_Dll.exe C_With_Dll.c DLL_Test.dll # link the programe with dll
```

* [Windows中DLL文件的意义及其作用 ](https://www.cnblogs.com/jpfss/p/9415136.html)

* [Building and using DLLs in C](https://www.codementor.io/@a_hathon/building-and-using-dlls-in-c-d7rrd4caz)

* [c语言创建dll，c语言调用dll](https://blog.csdn.net/bejustice/article/details/38294253)
* [C语言dll文件的说明以及生成、使用方法](https://blog.csdn.net/qq_30460949/article/details/90543803)

以下是用Codeblocks创建DLL项目自动编译的脚本（作为参考）：

```
mingw32-g++.exe -Wall -DBUILD_DLL -g  -c E:\C_Program\DLL_Test\main.cpp -o obj\Debug\main.o
mingw32-g++.exe -shared -Wl,--output-def=bin\Debug\libDLL_Test.def -Wl,--out-implib=bin\Debug\libDLL_Test.a -Wl,--dll  obj\Debug\main.o  -o bin\Debug\DLL_Test.dll  -luser32
```

