#include <windows.h>
#include <stdio.h>

typedef int (*AddFunc)(int a, int b);

int main()
{
    HMODULE hDll = LoadLibrary(".\\bin\\DLL_Test.dll");
    if(hDll == NULL) return 0;
    AddFunc test = (AddFunc)GetProcAddress(hDll, "test");
    int r = test(4, 6); //add
    printf("CPP Dynamic Invode DLL: %d", r);
    system("pause");
    
    return 0;
}
