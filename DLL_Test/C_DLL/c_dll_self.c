/*
    无意中发现的调用方式，如果dll与exe不在同一目录，则会报丢失dll的错误。
    解决1：dll拷贝到相同目录
    解决2：将dll拷贝到系统路径下C:/Windows，或者是在系统变量path中配置的路径
*/
#include <windows.h>
#include <stdio.h>

extern int test(int a, int b);

int main()
{
    int r = test(3, 5); //add
    printf("Self Invode DLL function %d", r);
    system("pause");
    
    return 0;
}
