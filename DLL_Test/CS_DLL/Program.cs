﻿using System;
using System.Runtime.InteropServices;

namespace CS_DLL
{
    class Program
    {
        /* DLL Import with 1st method */
        [DllImport("msvcrt.dll")]
        public static extern int puts(string c);

        [DllImport("DLL_Test.dll", CallingConvention = CallingConvention.Cdecl)]
　　　　 public extern static int test(int a, int b);

        static void Main(string[] args)
        {
            puts("Test");
            int r = test(5, 3);
            Console.WriteLine("Hello World {0}", r);
        }
    }
}