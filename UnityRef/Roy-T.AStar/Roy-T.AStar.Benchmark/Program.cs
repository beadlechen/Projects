﻿using System;
using BenchmarkDotNet.Running;

namespace Roy_T.AStar.Benchmark
{
    public class Program
    {
        static void Main(string[] _)
        {
            var summary = BenchmarkRunner.Run<AStarBenchmark>();
            Console.WriteLine(summary);
            Console.ReadLine();
            //Console.WriteLine("123123");
        }
    }
}
