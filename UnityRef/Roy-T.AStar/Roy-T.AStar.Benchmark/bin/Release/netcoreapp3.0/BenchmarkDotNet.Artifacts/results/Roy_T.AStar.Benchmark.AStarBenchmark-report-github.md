``` ini

BenchmarkDotNet=v0.12.0, OS=Windows 7 SP1 (6.1.7601.0)
Intel Core i5-7500 CPU 3.40GHz (Kaby Lake), 1 CPU, 4 logical and 4 physical cores
Frequency=3328369 Hz, Resolution=300.4475 ns, Timer=TSC
.NET Core SDK=3.1.301
  [Host]     : .NET Core 3.1.5 (CoreCLR 4.700.20.26901, CoreFX 4.700.20.27001), X86 RyuJIT
  DefaultJob : .NET Core 3.1.5 (CoreCLR 4.700.20.26901, CoreFX 4.700.20.27001), X86 RyuJIT


```
|                         Method |            Mean |         Error |       StdDev |
|------------------------------- |----------------:|--------------:|-------------:|
|                      GridBench |    228,074.1 ns |   1,592.61 ns |  1,489.73 ns |
|                     GridBench2 |  5,763,527.5 ns |  28,233.89 ns | 25,028.61 ns |
|                     GridBench3 |    618,297.1 ns |   3,422.32 ns |  3,201.24 ns |
|              GridWithHoleBench |        549.3 ns |       1.58 ns |      1.40 ns |
|       GridWithRandomHolesBench |    170,912.7 ns |   1,251.46 ns |  1,109.39 ns |
|      GridWithRandomLimitsBench | 16,329,978.5 ns |  96,445.34 ns | 85,496.28 ns |
| GridWithUnreachableTargetBench |  8,558,487.9 ns |  38,777.03 ns | 34,374.83 ns |
|          GridWithGradientBench | 21,021,033.0 ns | 105,128.83 ns | 93,193.97 ns |
