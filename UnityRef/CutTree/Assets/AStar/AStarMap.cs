﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ElementType
{
    Wall = 1,
    Tree = 2,
}

[Serializable]
public struct Pos
{
    public int x;
    public int y;

    public static Pos None = new Pos(-1, -1);

    public Pos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Pos operator +(Pos p1, Pos p2){
        return new Pos(p1.x + p2.x, p1.y + p2.y);
    }

    public static Pos operator -(Pos p1, Pos p2){
        return new Pos(p1.x - p2.x, p1.y - p2.y);
    }

    public float NormalizePercent(){
        float ls = x * x + y * y;
        return 1.0f / (float)Math.Sqrt((double)ls);
    }

    public static float Distance(Pos p1, float x, float y){
        var dx = p1.x - x;
        var dy = p1.y - y;
        return Mathf.Sqrt(dx*dx + dy*dy);
    }

    public override string ToString()
    {
        return $"Pos: x={x}, y={y}";
    }
    
}

[Serializable]
public class Element
{
    public ElementType type;
    public Pos pos;

    public Element(ElementType type, Pos pos){
        this.type = type;
        this.pos = pos;
    }

    public Element(Pos pos){
        this.pos = pos;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (!(obj is Element))
            return false;

        var obj2 = obj as Element;
        return this.pos.Equals(obj2.pos);
    }

    public override int GetHashCode()
    {
        return pos.GetHashCode();
    }
}

public class AStarMap : MonoBehaviour, IPointerClickHandler
{
    public List<Element> elements = new List<Element>();
    public Dictionary<Pos, MapElement> elementDic;
    public int width = 10;
    public int height = 10;
    public float scale = 1; // 格子比例

    //Single Instance 
    private static AStarMap _instance;

    public static AStarMap Instance { get { return _instance; } }

    public AStarAgent Player { get; set; }

    public AStarAgent[] agents;
    private AStarSearch m_search = new AStarSearch();

    public Tree treePrefab;

    public static Pos[] DIRS = {
        new Pos(1, 0), new Pos(0, -1), new Pos(-1, 0), new Pos(0, 1)
    };

    void Awake(){
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        elementDic = new Dictionary<Pos, MapElement>();
        foreach(var ele in elements){
            if(ele.type == ElementType.Tree){
                Tree tree = Instantiate<Tree>(treePrefab, this.transform);
                tree.Pos = ele.pos;

                elementDic.Add(ele.pos, tree);
            }
        }

        foreach(var agent in agents){
            agent.BindMap(this);
        }

        Player = agents[0];
    }

    void Update()
    {
        foreach(var agent in agents){
            // 若存在指定目标 则搜索目标位置
            // 为什么不直接在agent里进行搜索请求
            // 主要是担心后续太多agent需要进行移动操作，故在此进行管控 一帧只允许x个寻路计算
            agent.TrySearch();
            // 这里开始进行移动
            agent.Move(Time.deltaTime);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var position = eventData.pointerCurrentRaycast.worldPosition;
        Debug.Log("AStarMap OnPointerClick: " + position);

        var pos = TranslatePos(position.x, position.y);

        // Debug.Log("AStarMap OnPointerClick" + x + ":" + y);
        Player.MoveTo(pos);
    }

    public AStarAgent AddAgent(){
        var agent = new AStarAgent();
        agent.BindMap(this);
	    return agent;
    }

    public Pos TranslatePos(float x, float y){
        x /= this.scale;
        y /= this.scale;
        return new Pos((int)x, (int)y);
    }

    public Vector3 TranslateRealPos(Pos p){
        return new Vector3(p.x * this.scale, p.y * this.scale, 0);
    }

    //判断是否在格子内
    public bool InBounds(Pos pos){
        return pos.x >= 0 && pos.x < this.width 
                &&  pos.y >=0 && pos.y < this.height;
    }

    //是否是可通行的
    public bool Passable(Pos pos){
        if(!elementDic.ContainsKey(pos))
            return true;

        var ele = elementDic[pos];
        return ele.type != ElementType.Wall 
                && ele.type != ElementType.Tree;
    }

    //路径搜索
    public List<Pos> Search(Pos start, Pos end){
        //目标位置是否是墙

        //是否在范围内 ...

        return m_search.Search(this, start, end);
    }


    //寻找周围一圈的格子
    public void Neighbors(Pos pos, Action<Pos> cb, bool reverse = false){
        for(int i=0; i < DIRS.Length; i++){
            var idx = reverse ? DIRS.Length - i - 1 : i;
            var tar = pos + DIRS[idx];
            if(InBounds(tar) && Passable(tar)){
                cb(tar);
            }
        }
    }

    //计算格子的消耗 这里主要是允许后续可以有不同消耗的地块
    public int Cost(Pos pos){
        return 1;
    }

    public void RemoveElement(Pos pos){
        if(elementDic.ContainsKey(pos)){
            elementDic.Remove(pos);
        }
    }

    public List<Tree> GetTrees(Vector3 pos, Vector3 dir, float size){
        List<Tree> results = new List<Tree>();

        dir = dir.normalized * size;
        dir += pos;

        var start = TranslatePos(pos.x, pos.y);
        var end = TranslatePos(dir.x, dir.y);

        Debug.Log($"Get Trees : {pos}, {dir}, {start}, {end}");

        var startX = Math.Min(start.x, end.x);
        var endX = Math.Max(start.x, end.x);
        var startY = Math.Min(start.y, end.y);
        var endY = Math.Max(start.y, end.y);

        for(int i=startX; i <= endX; i++){
            for(int j=startY; j <= endY; j++){
                var p = new Pos(i, j);

                // Debug.Log("element type :" + p);
                if(!elementDic.ContainsKey(p))
                    continue;

                var ele = elementDic[p];
                if(ele.type == ElementType.Tree)
                    results.Add(ele as Tree);
            }
        }

        return results;
    }

}

