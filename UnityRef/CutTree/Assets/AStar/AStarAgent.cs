﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarAgent : MonoBehaviour
{
    public AStarMap m_map;

    private Pos m_targetPos = Pos.None;
    private float m_stopDis;

    private List<Pos> m_path;
    private Pos m_next_pos;

    private Animator m_animator;

    void Start(){
        // Debug.Log("当前位置：" + GetCurrentPos());

        m_animator = GetComponent<Animator>();
    }

    public void BindMap(AStarMap map){
        this.m_map = map; 
    }

    public void MoveTo(Pos pos, float stopDis = 0){
        m_targetPos = pos;
        m_stopDis = stopDis;
    }

    public void Move(float dt){
        if(m_path == null || m_path.Count == 0)
            return;

        var tarPos = m_path[0];
        
        var curPos2 = transform.position / m_map.scale;
        var dis = Pos.Distance(tarPos, curPos2.x, curPos2.y);
        if(m_path.Count == 1){
            if(dis < 0.4){
                m_animator.SetFloat("speed", 0);

                m_path = null;
                return;
            }
        }else{
            if(dis < 2){
                m_path.RemoveAt(0);
                tarPos = m_path[0];
            }
        }

        m_animator.SetFloat("speed", 1);

        var tarRealPos = new Vector3(tarPos.x, tarPos.y, 0) * m_map.scale;
        var dir = tarRealPos - transform.position;
        var curPos = transform.position;
        //更新位置
        transform.position += dir.normalized * dt * 5;

        //旋转角度
        // Debug.Log($"dir: {dir}, {tarPos}, {dis}");
        var targetR = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.y));
        targetR = Quaternion.Lerp(transform.localRotation, targetR, dt * 10);
        transform.localRotation = targetR;

        // Debug.Log("移动到目标点：" + GetCurrentPos());
    }

    //获取当前位置
    public Pos GetCurrentPos(){
        var m_currentPos = transform.position;
	    return m_map.TranslatePos(m_currentPos.x, m_currentPos.y);
    }

    //尝试进行寻路
    public bool TrySearch(){
        // 如果没有目标位置就别寻路了
        if(m_targetPos.Equals(Pos.None))
            return false;

        var currentPos = GetCurrentPos();
        // Debug.Log($"current: {currentPos.x}, {currentPos.y}");
        m_path = m_map.Search(currentPos, m_targetPos);
        // m_path = new List<Pos> {
        //     new Pos(1, 0), new Pos(1, 1), new Pos(2, 1), new Pos(2, 2), 
        //     new Pos(3, 2), new Pos(3, 3), new Pos(4, 3), new Pos(4, 4),
        // };

        // foreach(var p in m_path){
        //     Debug.Log("ppp:" + p);
        // }

        m_targetPos = Pos.None;

        return false;
    }

}
