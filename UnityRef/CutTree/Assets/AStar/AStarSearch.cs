﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarSearch
{
    private NodeList nodes;
    private Dictionary<Pos, Pos> pathMap;

    private int MaxLoop = 1000;

    public AStarSearch(){
        this.nodes = new NodeList();
        this.pathMap = new Dictionary<Pos, Pos>();
    }

    public List<Pos> Search(AStarMap map, Pos start, Pos end){
        nodes.Clear();
        pathMap.Clear();

        // Debug.Log($"Search : start {start}, end {end}");

        nodes.Insert(new PathNode(start));

        bool hasFind = false;
        bool reverse = false;

        while(nodes.OpenCount > 0){
            var current = nodes.PopToClosed();

            // Debug.Log("search node:" + current.pos);

            if(current.pos.Equals(end)){
                hasFind = true;
                break;
            }
            map.Neighbors(current.pos, p => {
                var newCost = current.costSoFar + map.Cost(p);
                var costRemain = Heuristic(p, end);
                var newNode = new PathNode(p, newCost, costRemain);

                if(nodes.Insert(newNode)){
                    pathMap[p] = current.pos;
                }
            }, reverse);

            reverse = !reverse;
        }

        if(!hasFind)
            return null;

        List<Pos> path = new List<Pos>();

        var currentPos = end;
        // Debug.Log("currentPos:" + currentPos);
        while(!currentPos.Equals(start)){
            // Debug.Log("ADD path:" + currentPos);
            path.Add(currentPos);
            currentPos = pathMap[currentPos];
        }
        path.Reverse();

        // Debug.Log("1111:" + loop);

        return path;
    }

    public int Heuristic(Pos a, Pos b){
        var dis = b - a;
        return Math.Abs(dis.x) + Math.Abs(dis.y);
    }
}

public class PathNode {
    public Pos pos;
    public int costSoFar;
    public int costRemain;

    public int ExpectedTotalTime { get; }

    public PathNode(Pos pos, int costSoFar = 0, int costRemain = 0){
        this.pos = pos;
        this.costSoFar = costSoFar;
        this.costRemain = costRemain;
        this.ExpectedTotalTime = costSoFar + costRemain;
    }
    
    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (!(obj is PathNode))
            return false;

        var obj2 = obj as PathNode;
        return this.pos.Equals(obj2.pos);
    }

    public override int GetHashCode()
    {
        return pos.GetHashCode();
    }
}