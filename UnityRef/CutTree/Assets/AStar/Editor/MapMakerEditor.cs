﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MapEditor {

    [CustomEditor(typeof(AStarMap))]
    public class MapMakerEditor : Editor
    {
        public bool noLine;
        public bool showGui = true;

        public AStarMap _target
        {
            get { return target as AStarMap; }
        }

        public void AddWall(int x, int y)
        {
            DelElement(x, y);
            var p = new Pos(x, y);
            _target.elements.Add(new Element(ElementType.Wall, p));
        }

        public void DelElement(int x, int y)
        {
            var p = new Pos(x, y);
            var e = new Element(ElementType.Wall, p);
            if (_target.elements.Contains(e))
                _target.elements.Remove(e);
        }

        public void AddTree(int x, int y)
        {
            DelElement(x,y);
            var p = new Pos(x, y);
            _target.elements.Add(new Element(ElementType.Tree, p));
        }

        private void OnSceneGUI()
        {
            var scale = _target.scale;
            var width = _target.width;
            var height = _target.height;

            if (Event.current.type == EventType.MouseDown
                && Event.current.button == 0)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, Vector2.zero);
                Debug.Log($"111111：{ray.origin}, {hit.point}");
                if (hit.collider != null)
                {
                    var x = Mathf.FloorToInt(hit.point.x / _target.scale);
                    var y = Mathf.FloorToInt(hit.point.y / _target.scale);
                    // Debug.Log($"33333333333: {x}, {y}, {hit.point}, {hit.collider.name}");
                    if (Event.current.alt)
                    {
                        DelElement(x, y);
                    }
                    else if (Event.current.shift)
                    {
                        AddTree(x, y);
                    }else
                    {
                        AddWall(x, y);
                    }
                }
            }

            if (showGui)
            {
                if (!noLine)
                {
                    for (var w = 0; w < width; w++)
                    {
                        var wl2 = new Vector3((w + 1) * _target.scale, 0, 0);
                        Handles.DrawLine(wl2, wl2 + Vector3.up * height * _target.scale);
                    }
                    //横线
                    for (var h = 0; h < height; h++)
                    {
                        var hl2 = new Vector3(0, (h + 1) * scale, 0);
                        Handles.DrawLine(hl2, hl2 + Vector3.right * width * _target.scale);
                    }
                }

                foreach (var ele in _target.elements)
                {
                    Handles.color = ele.type == ElementType.Tree ? Color.green : Color.red;
                    var center = new Vector3((ele.pos.x + 0.5f) * scale, (ele.pos.y + 0.5f) * scale, 0);
                    Handles.DrawWireCube(center, Vector3.one * 0.2f);
                }

                Handles.color = Color.white;
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            showGui = EditorGUILayout.Toggle("showGui", showGui);
            noLine = EditorGUILayout.Toggle("noLine", noLine);
        }

        public void Export()
        {
            // foreach (var dataForest in _target.data.forests)
            // {
            //     if(_target.data.walls.Contains(dataForest))
            //         Debug.Log("重复对象" + dataForest);
            // }
            
            // var msg = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/External/AStar/AStarUtil/template.txt");
            // var msgText = msg.text;
            // msgText = msgText.Replace("%scale%", _target.data.scale.ToString());
            // msgText = msgText.Replace("%width%", _target.data.width.ToString());
            // msgText = msgText.Replace("%height%", _target.data.height.ToString());
            // var wallx = "{";
            // var wally = "{";
            // for (var i = 0; i < _target.data.walls.Count; i++)
            // {
            //     var dataWall = _target.data.walls[i];
            //     wallx += dataWall.x;
            //     wally += dataWall.y;
            //     if (i < _target.data.walls.Count)
            //     {
            //         wallx += ",";
            //         wally += ",";
            //     }
            // }

            // wallx += "}";
            // wally += "}";
            
            // var forestx = "{";
            // var foresty = "{";
            // for (var i = 0; i < _target.data.forests.Count; i++)
            // {
            //     var forest = _target.data.forests[i];
            //     forestx += forest.x;
            //     foresty += forest.y;
            //     if (i < _target.data.forests.Count)
            //     {
            //         forestx += ",";
            //         foresty += ",";
            //     }
            // }

            // forestx += "}";
            // foresty += "}";
            
            // msgText = msgText.Replace("%wallx%", wallx);
            // msgText = msgText.Replace("%wally%", wally);
            
            // msgText = msgText.Replace("%forests_x%", forestx);
            // msgText = msgText.Replace("%forests_y%", foresty);
            
            // var path = Application.dataPath + @"\Game\Lua\Logic\Square\Maps\" + _target.data.mapName + ".txt";
            // if (!File.Exists(path))
            //     File.Delete(path);
            // Debug.Log(path);
            // Debug.Log(msgText);

            // File.WriteAllText(path, msgText);
            // AssetDatabase.Refresh();
        }
    }

}
