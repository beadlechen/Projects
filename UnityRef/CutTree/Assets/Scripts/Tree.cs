﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MapElement
{
    public override ElementType type {
        get {
            return ElementType.Tree; 
        }
    }
}
