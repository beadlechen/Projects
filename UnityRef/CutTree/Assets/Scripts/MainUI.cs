﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
    public Button cutBtn;
    // Start is called before the first frame update
    void Start()
    {
        cutBtn.onClick.AddListener(() => {
            MainController.Instance.Player.DoCut();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
