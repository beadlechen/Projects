﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Animator m_animator;

    private PlayerState m_cur_state = PlayerState.IDLE;
    private float m_cut_time = 0;

    void Start(){
        m_animator = GetComponent<Animator>();
    }

    void Update(){
        if(Input.GetKeyDown(KeyCode.Space)){
            DoCut();
        }

        if(m_cut_time > 0){
            m_cut_time -= Time.deltaTime;

            if(m_cut_time <= 0){
                SetState(PlayerState.IDLE);
            }
        }
    }
    public void SetState(PlayerState state){
        var speed = 0;
        var cut = false;

        switch(state){
            case PlayerState.RUN:
                if(m_cur_state != PlayerState.IDLE)
                    return;
                speed = 1;
                break;

            case PlayerState.CUT:
                if(m_cur_state != PlayerState.IDLE)
                    return;
                cut = true;

                CutTrees();
                break;
        }

        m_animator.SetFloat("speed", speed);
        m_animator.SetBool("cut", cut);
    }

    public void DoCut(){
        SetState(PlayerState.CUT);
        m_cut_time = 0.5f;
    }

    private void CutTrees(){
        List<Tree> trees = AStarMap.Instance.GetTrees(transform.position, transform.forward, 2);
        foreach(var tree in trees){
            tree.Despawn();
        }
    }
}

public enum PlayerState {
    IDLE = 1,
    RUN = 2,
    CUT = 3,
}
