﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform target;
    public SpriteRenderer map;
    public float smooth = 8f;
    private Camera m_camera;

    void Awake(){
        m_camera = GetComponent<Camera>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("map size:" + map.size);
        var height = 2 * m_camera.orthographicSize;
        var width = height * m_camera.aspect;

        Debug.Log("aaaa:" + height + ":" +width);
    }

    // Update is called once per frame
    void Update()
    {
        var mapSize = map.size;
        var height = 2 * m_camera.orthographicSize;
        var width = height * m_camera.aspect;
        var tarPos = target.position;
        var curPos = transform.position;

        curPos.x = Mathf.Clamp(tarPos.x, width / 2, mapSize.x - width / 2);
        curPos.y = Mathf.Clamp(tarPos.y, height / 2, mapSize.y - height / 2);
        
        transform.position = Vector3.Lerp(transform.position, curPos, smooth * Time.deltaTime);
    }
}
