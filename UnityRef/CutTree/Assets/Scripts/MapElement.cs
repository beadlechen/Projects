﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class MapElement : MonoBehaviour
{
    public virtual ElementType type { get; }
    private Pos m_pos;

    public Pos Pos { 
        get {
            return this.m_pos;
        }
        
        set {
            this.m_pos = value;
            var realPos = AStarMap.Instance.TranslateRealPos(value);
            realPos.z = -0.1f; //高一点，怕被挡住
            transform.position = realPos;
        }
    }

    public void Despawn(){
        AStarMap.Instance.RemoveElement(m_pos);

        Destroy(this.gameObject);
    }

}
