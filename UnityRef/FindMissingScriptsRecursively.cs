using UnityEngine;
using UnityEditor;
using System.IO;

public class FindMissingScriptsRecursively : EditorWindow 
{
    static int go_count = 0, components_count = 0, missing_count = 0;
 
    [MenuItem("Window/FindMissingScriptsRecursively")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(FindMissingScriptsRecursively));
    }
 
    public void OnGUI()
    {
        if (GUILayout.Button("Find Missing Scripts in selected GameObjects"))
        {
            FindInSelected();
        }
    }
    private static void FindInSelected()
    {
        go_count = 0;
        components_count = 0;
        missing_count = 0;
        
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        Debug.Log("选中路径：" + path);
        string[] files = Directory.GetFiles(path, "*.prefab", SearchOption.AllDirectories);
        for (int i = 0; i < files.Length; i++)
        {
            var p = (float) i / files.Length;
            GameObject gameObject = AssetDatabase.LoadAssetAtPath<GameObject>(files[i]);

//            Debug.Log("选中对象：" + gameObject.name);
            
            FindInGO(gameObject);
            
            EditorUtility.DisplayProgressBar("检查:", "进度:" + p * 100 + "%", p);

//            EditorUtility.SetDirty(gameObject);
//
//            AssetDatabase.SaveAssets();
//
//            AssetDatabase.Refresh();

        }
        
        EditorUtility.ClearProgressBar();
        
        
        Debug.Log(string.Format("Searched {0} GameObjects, {1} components, found {2} missing", go_count, components_count, missing_count));
    }
 
    private static void FindInGO(GameObject g)
    {
        go_count++;
        Component[] components = g.GetComponents<Component>();
        for (int i = 0; i < components.Length; i++)
        {
            components_count++;
            if (components[i] == null)
            {
                missing_count++;
                string s = g.name;
                Transform t = g.transform;
                while (t.parent != null) 
                {
                    s = t.parent.name +"/"+s;
                    t = t.parent;
                }
                Debug.Log (g.name + ":::::" + s + " has an empty script attached in position: " + i, g);
            }
        }
        // Now recurse through each child GO (if there are any):
        foreach (Transform childT in g.transform)
        {
            //Debug.Log("Searching " + childT.name  + " " );
            FindInGO(childT.gameObject);
        }
    }
}